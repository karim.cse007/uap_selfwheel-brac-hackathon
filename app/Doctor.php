<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','bmdc_code',
    ];
    public function city(){
        return $this->belongsTo(City::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function specialist(){
        return $this->belongsTo(Specialist::class);
    }
    public function articles(){
        return $this->hasMany(Article::class);
    }
}
