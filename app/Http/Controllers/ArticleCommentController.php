<?php

namespace App\Http\Controllers;

use App\Article;
use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ArticleCommentController extends Controller
{
    public function CommentStore(Request $request){
        $bal = Validator::make($request->all(),[
           'article_id'=>'required|integer|not_in:0|exists:articles,id',
           'comment'=>'required|string',
        ]);
        if ($bal->fails()){
            return response()->json('Invalid request');
        }
        $comment = new Comment();
        $comment->user_id=auth()->id();
        $comment->article_id=$request->article_id;
        $comment->comment = $request->comment;
        $comment->save();
        return response()->json('Thank you for your comment');
    }
    public function reload($id){
        $article = Article::findOrFail($id);
        $comments = $article->comments;
        return view('articleComment',compact('comments'));

    }
}
