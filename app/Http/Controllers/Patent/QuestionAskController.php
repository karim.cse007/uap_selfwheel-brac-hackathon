<?php

namespace App\Http\Controllers\Patent;

use App\Http\Controllers\Controller;
use App\Question;
use App\QuestionComment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class QuestionAskController extends Controller
{

    public function store(Request $request)
    {
        $val = Validator::make($request->all(),[
           'question'=>'required|string',
        ]);
        if ($val->fails()){
            return response()->json('Invalid request',200);
        }
        if (auth()->user()->role->id != 2){
            return response()->json('Invalid request',200);
        }
        $question = new Question();
        $question->user_id = auth()->id();
        $question->description = $request->question;
        $question->save();
        return response()->json('Successfully posted your question',201);
    }
    public function CommentStore(Request $request){
        $val = Validator::make($request->all(),[
           'question_id'=>'required|integer|not_in:0|exists:questions,id',
           'comment'=>'required|string',
        ]);
        if ($val->fails()){
            return response()->json('Invalid request',200);
        }
        $comment = new QuestionComment();
        $comment->user_id = auth()->id();
        $comment->question_id = $request->question_id;
        $comment->comment = $request->comment;
        $comment->save();
        return response()->json('Thank you for you comment',201);
    }
}
