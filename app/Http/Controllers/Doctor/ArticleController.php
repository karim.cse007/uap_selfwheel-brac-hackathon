<?php

namespace App\Http\Controllers\Doctor;

use App\Article;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class ArticleController extends Controller
{
    public function store(Request $request){

        $val = Validator::make($request->all(),[
           'title'=>'required|string|max:191',
           'article'=>'required|string',
           'image'=>'image|mimes:jpg,jpeg,png',
        ]);
        if ($val->fails()){
            return redirect()->back()->with('error','Invalid request');
        }
        $image = $request->file('image');

        if (isset($image)){
            $imageName = Str::slug($request->title).'-'.uniqid().'.'.$image->getClientOriginalExtension();
            if (!Storage::disk('public')->exists('article')){
                Storage::disk('public')->makeDirectory('article');
            }
            $img = Image::make($image)->resize(800,600)->save();
            Storage::disk('public')->put('article/'.$imageName,$img);
        }
        //dd($imageName);
        $article = new Article();
        $article->doctor_id = auth()->user()->doctor->id;
        $article->title = $request->title;
        $article->description = $request->article;
        if (isset($imageName)){
            $article->image = $imageName;
        }
        $article->save();
        return redirect()->back();

    }
}
