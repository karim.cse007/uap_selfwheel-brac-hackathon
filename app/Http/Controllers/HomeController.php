<?php

namespace App\Http\Controllers;

use App\Article;
use App\Doctor;
use App\Question;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        #$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $articles = Article::OrderBy('id','DESC')->get();
        return view('welcome',compact('articles'));
    }
    public function doctor()
    {
        $doctors= Doctor::all();
        return view('doctor',compact('doctors'));
    }
    public function questionPage()
    {
        $questions = Question::orderBy('id','DESC')->get();
        return view('questionPage',compact('questions'));

    }
    public function doctorInfo($id)
    {
        $doctor = Doctor::findOrFail($id);
        //dd($doctor);
        return view('doctorDetails',compact('doctor'));
    }
    public function singleArticle($id)
    {
        $article = Article::findOrFail($id);
        return view('singleArticle',compact('article'));
    }
}
