<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public function Comments(){
        return $this->hasMany(QuestionComment::class);
    }
}
