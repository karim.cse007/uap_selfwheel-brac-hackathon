<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDoctorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bmdc_code')->unique();
            $table->integer('user_id')->unsigned();
            $table->date('date_of_birth')->nullable();
            $table->double('fee')->nullable();
            $table->integer('experience_id')->unsigned()->nullable();
            $table->integer('specialist_id')->unsigned()->nullable();
            $table->integer('city_id')->unsigned()->nullable();
            $table->text('address')->nullable();
            $table->boolean('is_active')->default(false);
            $table->timestamps();
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('experience_id')
                ->references('id')->on('experiences')
                ->onDelete('cascade');
            $table->foreign('specialist_id')
                ->references('id')->on('specialists')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctors');
    }
}
