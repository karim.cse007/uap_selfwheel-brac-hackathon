<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'id'=>1,
            'name'=>'Admin',
        ]);
        DB::table('roles')->insert([
            'id'=>2,
            'name'=>'Doctor',
        ]);
        DB::table('roles')->insert([
            'id'=>3,
            'name'=>'Patient',
        ]);
    }
}
