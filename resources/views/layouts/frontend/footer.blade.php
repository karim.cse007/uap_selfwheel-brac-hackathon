<footer class="page_footer template_footer ds ms parallax overlay_color section_padding_top_50 section_padding_bottom_50 columns_padding_25">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-push-6 text-center">
                <div class="widget widget_text widget_about">
                    <div class="logo logo_with_text bottommargin_10">
                        <img src="{{asset('assets/frontend/images/logo.png')}}" alt=""> <span class="logo_text">
                            Self Recovery
                            <small class="highlight4">Recover yourself from home</small>
                        </span> </div>
                    <p>The mission of Our Community Center is to enhance and sustain the health of people around the world.</p>
                    <p class="topmargin_25">
                        <a class="fab fa-facebook" href="#" title="Facebook"></a>
                        <a class="fab fa-twitter" href="#" title="Twitter"></a>
                        <a class="fab fa-google-plus-g" href="#" title="Google Plus"></a>
                        <a class="fab fa-linkedin" href="#" title="Linkedin"></a>
                        <a class="fab fa-youtube" href="#" title="Youtube"></a>
                    </p>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-pull-6 text-center">
                <div class="widget widget_text">
                    <h3 class="widget-title">Our Contacts</h3>
                    <ul class="list-unstyled greylinks">
                        <li class="media">
                            <i class="fa fa-map-marker highlight rightpadding_5" aria-hidden="true"></i>
                            05 Senpara Parbata, Mirpur 10, Dhaka </li>
                        <li class="media"> <i class="fa fa-phone highlight rightpadding_5" aria-hidden="true"></i>
                            8 (800) 923 4567 (operator) </li>
                        <li class="media"> <i class="fa fa-pencil highlight rightpadding_5" aria-hidden="true"></i>
                            <a href="mailto:diversify@example.com">info@selfrecovery.com</a> </li>
                        <li class="media"> <i class="fa fa-clock-o highlight rightpadding_5" aria-hidden="true"></i>
                            Sun-Fri: 9:00-19:00, Sat: 10:00-17:00 </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<section class="ds ms parallax page_copyright overlay_color section_padding_top_10 section_padding_bottom_30">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <p>&copy; Copyright 2019. All Rights Reserved.</p>
            </div>
        </div>
    </div>
</section>
