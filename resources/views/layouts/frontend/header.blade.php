
<header class="page_header header_white toggler_xs_right columns_margin_0">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 display_table">
                <div class="header_left_logo display_table_cell"> <a href="/" class="logo logo_with_text">
                        <img src="{{asset('assets/frontend/images/logo.png')}}" alt="">
                        <span class="logo_text">
                                Self Recovery
                                <small class="highlight4">Recover yourself from home</small>
                            </span>
                    </a> </div>
                <div class="header_mainmenu display_table_cell text-center">
                    <!-- main nav start -->
                    <nav class="mainmenu_wrapper">
                        <ul class="mainmenu nav sf-menu">
                            <li class="{{Request::is('/')?'active':''}}"> <a href="/">Home</a></li>
                            <li class="{{Request::is('doctor')?'active':''}}"> <a href="{{route('doctor')}}">Doctor</a></li>
                            <li class="{{Request::is('message/page')?'active':''}}"> <a href="{{route('message.page')}}">Message</a></li>
                            <li class="{{Request::is('question/answer')?'active':''}}"> <a href="{{route('question.answer')}}">ASK QUESTION</a></li>
                            @if (auth()->check() and isset(auth()->user()->role) and auth()->user()->role->id == 2)
                                <li class="{{Request::is('doctor/profile')?'active':''}}">
                                    <a href="{{route('doctor.profile')}}">Profile</a>
                                </li>
                            @elseif (auth()->check() and isset(auth()->user()->role) and auth()->user()->role->id == 3)
                                <li class="{{Request::is('doctor/profile')?'active':''}}">
                                    <a href="#">Profile</a>
                                </li>
                            @elseif (auth()->check() and isset(auth()->user()->role) and auth()->user()->role->id == 1)
                                <li class="{{Request::is('doctor/profile')?'active':''}}">
                                    <a href="#">Dashboard</a>
                                </li>
                            @endif
                            @guest()
                            <li class="{{Request::is('login')?'active':''}}"> <a href="{{route('login')}}">Login</a></li>
                            <li class="{{Request::is('register')?'active':''}}"> <a href="{{route('register')}}">Register</a> </li>
                            @else
                                <li> <a href="javascript:void(0);" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a> </li>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            @endguest
                        </ul>
                    </nav>
                    <!-- eof main nav -->
                    <!-- header toggler -->
                    <span class="toggle_menu"><span>
                            </span>
                        </span>
                </div>
            </div>
        </div>
    </div>
</header>

