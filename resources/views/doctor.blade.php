@extends('layouts.frontend.app')

@section('content')
    <section class="ls page_portfolio section_padding_top_100 section_padding_bottom_100">
        <div class="container">
            <div class="row flex-wrap columns_margin_bottom_20">
                @foreach($doctors as $doctor)
                    <div class="col-sm-6 col-md-4">
                    <article class="vertical-item content-padding big-padding with_border bottom_color_border loop-color text-center">
                        <div class="item-media">
                            <img src="{{asset('assets/frontend/images/faces/04.jpg')}}" alt="">
                        </div>
                        <div class="item-content">
                            <header class="entry-header">
                                <h3 class="entry-title small bottommargin_0">
                                    <a href="{{route('doctor.info',$doctor->id)}}">{{$doctor->user->name}}</a>
                                </h3>
                            </header>
                            <p class="member-social greylinks">{{ $doctor->specialist?$doctor->specialist->name:'' }}</p>
                        </div>
                    </article>
                </div>
                @endforeach
            </div>
        </div>
    </section>

@endsection
