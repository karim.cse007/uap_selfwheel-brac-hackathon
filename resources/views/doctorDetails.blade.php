@extends('layouts.frontend.app')

@section('content')
    <section class="ls section_padding_top_100 section_padding_bottom_80 columns_padding_30">
        <div class="container">
            <div class="row">
                <div class="col-sm-5">
                    <article class="vertical-item content-padding big-padding with_border bottom_color_border text-center">
                        <div class="item-media">
                            <img src="{{asset('assets/frontend/images/faces/02.jpg')}}" alt="">
                        </div>
                        <div class="item-content">
                            <header class="entry-header">
                                <h1 class="entry-title small bottommargin_0"> Adeline Barnett </h1>
                                <span class="small-text highlight">Cardiologist </span>
                            </header>
                            <p class="member-social greylinks"></p>
                        </div>
                    </article>
                </div>
                <div class="col-sm-7">
                    <p>Kielbasa pancetta sirloin tail meatball, corned beef doner shoulder cow bacon jerky leberkas. Meatball tail landjaeger, cupim alcatra pancetta biltong frankfurter picanha. Turducken pork loin tail pastrami strip steak. Sausage ground round chuck
                        andouille, pork belly bacon ham hock picanha cow spare ribs meatball frankfurter t-bone salami.</p>
                    <p>Landjaeger burgdoggen bresaola picanha, chuck swine cow frankfurter biltong chicken leberkas brisket sausage flank pig.Brisket frankfurter ham hock sausage strip steak flank ground round ball tip, doner corned beef t-bone cow jowl bacon turkey.</p>
                    <ul class="list2 color2 checklist greylinks">
                        <li> Lorem ipsum dolor sit amet </li>
                        <li> Sint animi non ut sed </li>
                        <li> Eaque blanditiis nemo </li>
                    </ul>
                    <p>Cow frankfurter tri-tip chuck flank jowl cupim chicken picanha turducken pork chop pig. Cupim pork biltong pork chop strip steak, hamburger beef ribs ground round. Cow shoulder meatloaf alcatra, ham pork loin shank jerky burgdoggen meatball cupim
                        kevin prosciutto ground round. </p>
                    <!-- Nav tabs -->
                    <!-- eof .tab-content -->
                </div>
                <div class="col-md-12">
                    <ul class="nav nav-tabs color2 topmargin_40" role="tablist">
                        <li class="active"><a href="#tab1" role="tab" data-toggle="tab">Chamber</a></li>
                        <li><a href="#tab2" role="tab" data-toggle="tab">Education History</a></li>
                        <li><a href="#tab3" role="tab" data-toggle="tab">Achievement</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content big-padding top-color-border color2 bottommargin_20">
                        <div class="tab-pane fade in active" id="tab1">
                            <h4>Chamber at Mipur:</h4>
                            <p> Ut wisi enim ad minim veniaquis nostrud exetation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Dutem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis
                                at vero eros et accumsan et iusto odio dignissim qui blandit. </p>
                            <h4>Chamber at Dhanmondi:</h4>
                            <p> Dutem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit. Praesent luptatum zzril delenit augue duis dolore te
                                feugait nulla facilisi. </p>
                        </div>
                        <div class="tab-pane fade" id="tab2">
                            <p class="progress-bar-title"> Skill Name </p>
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" data-transitiongoal="90"> <span>90%</span> </div>
                            </div>
                            <p class="progress-bar-title"> Skill Name </p>
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" data-transitiongoal="100"> <span>100%</span> </div>
                            </div>
                            <p class="progress-bar-title"> Skill Name </p>
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" data-transitiongoal="75"> <span>75%</span> </div>
                            </div>
                            <p class="progress-bar-title"> Skill Name </p>
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" data-transitiongoal="95"> <span>95%</span> </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab3">
                            <div class="tab-pane fade in active" id="tab1">
                                <h4>The Franklin Institute Awards:</h4>
                                <p> Ut wisi enim ad minim veniaquis nostrud exetation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Dutem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis
                                    at vero eros et accumsan et iusto odio dignissim qui blandit. </p>
                                <h4>Albert Einstein World Award of Science:</h4>
                                <p> Dutem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit. Praesent luptatum zzril delenit augue duis dolore te
                                    feugait nulla facilisi. </p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection
