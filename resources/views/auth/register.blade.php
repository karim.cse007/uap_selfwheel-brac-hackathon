@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row no-gutter">
            <div class="d-none d-md-flex col-md-4 col-lg-6 bg-image"></div>
            <div class="col-md-12 col-lg-12">
                <div class="login d-flex align-items-center py-5">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-9 col-lg-8 mx-auto pl-5 pr-5">
                                <h3 class="login-heading mb-4">New Doctor!</h3>
                                <form method="POST" action="{{ route('register') }}">
                                    @csrf
                                    <fieldset class="form-group">
                                        <label>Register Number</label>
                                        <div class="input-group">
                                            <span class="input-group-prepend">
                                              <span class="input-group-text">
                                                <i class="fa fa-user-circle"></i>
                                              </span>
                                            </span>
                                            <input type="text" id="bmdc_code" class="form-control @error('bmdc_code') is-invalid @enderror" name="bmdc_code" value="{{ old('bmdc_code') }}"  autocomplete="bmdc_code" autofocus placeholder="Enter Your BMDC number">
                                            @error('bmdc_code')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <small class="text-muted">ex. Bangladesh Medical & Dental Council Register number</small>
                                    </fieldset>
                                    <fieldset class="form-group">
                                        <label>Name</label>
                                        <div class="input-group">
                                            <span class="input-group-prepend">
                                              <span class="input-group-text">
                                                <i class="fa fa-user-circle"></i>
                                              </span>
                                            </span>
                                            <input type="text" id="name" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Enter Your Name">
                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </fieldset>
                                    <fieldset class="form-group">
                                        <label>Email</label>
                                        <div class="input-group">
                                            <span class="input-group-prepend">
                                              <span class="input-group-text">
                                                <i class="fa fa-envelope"></i>
                                              </span>
                                            </span>
                                            <input type="email" id="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="example@email.com">
                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </fieldset>
                                    <fieldset class="form-group">
                                        <label>Phone Number</label>
                                        <div class="input-group">
                                            <span class="input-group-prepend">
                                              <span class="input-group-text">
                                                <i class="fa fa-phone-square"></i>
                                              </span>
                                            </span>
                                            <input type="text" id="phone_number" value="{{ old('phone_number') }}" class="form-control @error('phone_number') is-invalid @enderror" name="phone_number" autofocus placeholder="Phone Number">

                                            @error('phone_number')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <small class="text-muted">ex. your phone number</small>
                                    </fieldset>
                                    <fieldset class="form-group">
                                        <label>Password</label>
                                        <div class="input-group">
                                            <span class="input-group-prepend">
                                              <span class="input-group-text">
                                                <i class="fa fa-unlock-alt" aria-hidden="true"></i>
                                              </span>
                                            </span>
                                            <input type="password" id="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Password">

                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <small class="text-muted">Minimum length 8</small>
                                    </fieldset>
                                    <fieldset class="form-group">
                                        <label>Confirm Password</label>
                                        <div class="input-group">
                                            <span class="input-group-prepend">
                                              <span class="input-group-text">
                                                <i class="fa fa-unlock-alt" aria-hidden="true"></i>
                                              </span>
                                            </span>
                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm password" required autocomplete="new-password">
                                        </div>
                                    </fieldset>
                                    <div class="custom-control custom-checkbox mb-3">
                                        <label
                                            for="notification {{ old('notification') ? 'checked' : '' }}"></label>
                                        <input class="custom-control-input" type="checkbox" name="notification" id="notification" {{ old('notification') ? 'checked' : '' }}>
                                        <label class="custom-control-label" for="notification">I accept the term and condition</label>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-block btn-lg btn-gradient text-uppercase font-weight-bold mb-2">
                                        {{ __('Register') }}
                                    </button>
                                    <div class="text-center pt-3">
                                        Already have an Account? <a class="font-weight-bold" href="{{ route('login') }}">Sign In</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
