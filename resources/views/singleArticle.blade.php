@extends('layouts.frontend.app')

@section('content')
    <section class="ls section_padding_top_110 section_padding_bottom_100 columns_padding_30">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-push-1">
                    <article class="vertical-item content-padding big-padding with_border single-post post">
                        @if ($article->image != null)
                            <div class="item-media-wrap">
                                <div class="entry-thumbnail item-media">
                                    <img src="{{asset('storage/article/'.$article->image)}}" alt="">
                                </div>
                            </div>
                        @endif
                        <div class="item-content">
                            <header class="entry-header">
                                <div class="entry-meta greylinks color4 inline-content">
                                    <span>
									<i class="fa fa-calendar highlight4 rightpadding_5" aria-hidden="true"></i>
									<time datetime="2017-10-03T08:50:40+00:00">{{ \Carbon\Carbon::parse($article->created_at)->format('d M, Y') }}</time>
								    </span>
                                    <span>
									<i class="fa fa-comment highlight4 rightpadding_5" aria-hidden="true"></i>
									<a href="blog-single-full.html#comments">{{$article->comments->count()}} comments</a>
								    </span>
                                </div>
                                <h1 class="entry-title"> {{$article->title}} </h1>
                            </header>
                            <div class="entry-content">
                                 {{ $article->description }}

                            </div>
                            <!-- .entry-content -->
                        </div>
                        <!-- .item-content -->
                    </article>
                    <div class="comments-area" id="comments-area">
                        @foreach($article->comments as $comment)
                            <ol class="comment-list">
                                <li class="comment even thread-even depth-1 parent">
                                    <article class="comment">
                                        <div class="comment-author"> <img class="media-object" alt="" src="{{asset('assets/frontend/images/faces/04.jpg')}}"> </div>
                                        <div class="comment-body">
                                            <div class="comment-meta darklinks">
                                                <a class="author_url" rel="external nofollow" href="#">{{$comment->user->name}}</a>
                                                <span class="comment-date">
                                                <i class="fa fa-calendar highlight4 rightpadding_5" aria-hidden="true"></i>
                                                <time datetime="2017-10-03T08:50:40+00:00">
                                                    {{$comment->created_at->diffForHumans()}}
                                                </time>
										    </span>
                                            </div>
                                            <div class="comment-text">
                                                <p>{{$comment->comment}}</p>
                                            </div>
                                            <span class="reply">
										    <a href="#respond">
											    <i class="fa fa-reply" aria-hidden="true"></i>
											<span class="sr-only">Reply</span> </a>
                                        </span>
                                        </div>
                                    </article>
                                <!-- .comment-body
                                <ol class="children">
                                    <li class="comment byuser odd alt depth-2 parent">
                                        <article class="comment">
                                            <div class="comment-author">
                                                <img class="media-object" alt="" src="{{asset('assets/frontend/images/faces/05.jpg')}}">
                                            </div>
                                            <div class="comment-body">
                                                <div class="comment-meta darklinks"> <a class="author_url" rel="external nofollow" href="#">Devin McGee</a>
                                                    <span class="comment-date">
													<i class="fa fa-calendar highlight4 rightpadding_5" aria-hidden="true"></i>
													<time datetime="2017-10-03T08:50:40+00:00">
														25 jan, 2018
													</time>
												</span> </div>
                                                <div class="comment-text">
                                                    <p>Shankle ham hock chuck pork chop capicola. Ham andouille beef chicken.</p>
                                                </div> <span class="reply">
												<a href="#respond">
													<i class="fa fa-reply" aria-hidden="true"></i>
													<span class="sr-only">Reply</span> </a>
														</span>
                                            </div>
                                        </article>
                                    </li>
                                </ol>
                                ----->
                                </li>
                                <!-- #comment-## -->
                            </ol>
                        @endforeach
                        <!-- .comment-list -->
                    </div>
                    @if (auth()->check())
                        <!-- #comments -->
                        <div class="comment-respond text-center" id="respond">
                            <h3>Write comment Now:</h3>
                            <h6 id="status"></h6>
                            <form class="comment-form" id="comment-form" method="post">
                                @csrf
                                <div class="row columns_padding_10">
                                    <div class="col-xs-12">
                                        <div class="form-group margin_0">
                                            <label for="comment">Comment</label>
                                            <textarea aria-required="true" rows="4" cols="45" name="comment" id="comment" class="form-control text-center" placeholder="write your Comment here ............."></textarea>
                                            <input name="article_id" value="{{$article->id}}" type="hidden">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-submit topmargin_30">
                                    <input type="submit" id="submit" name="submit" class="theme_button color3 min_width_button margin_0" />
                                </div>
                            </form>
                        </div>
                    @endif
                </div>
                <!--eof .col-sm-8 (main content)-->
            </div>
        </div>
    </section>
@endsection
@push('js')
    @if (auth()->check())
        <script>
            $(document).ready(function(){
                $('#comment-form').submit(function () {
                    $.post("<?php echo url(route('article.comment.store'))?>", $(this).serialize(),function (data) {
                        $('#status').html(data);
                        $('#comment').val('');
                        $('#comments-area').load("{{route('article.comment.reload',$article->id)}}").fadeIn('slow');
                    });
                    return false;
                });
            });
        </script>
    @endif
@endpush
