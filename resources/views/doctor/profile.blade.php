@extends('layouts.frontend.app')
@push('css')
    <link href="{{asset('assets/frontend/css/profile.css')}}" rel="stylesheet">
@endpush
@section('content')
    <!--title-bar start-->
    <section class="title-bar">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="left-title-text">
                        <h3>My Account</h3>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--title-bar end-->
    <!--my-account start-->
    <section class="my-account">
        <div class="profile-bg">
            <div class="my-Profile-dt" style="margin-top: 112px;">
                <div class="container">
                    <div class="row">
                        <div class="container">
                            <div class="profile-dpt">
                                <img src="{{asset('storage/profile/'.auth()->user()->image)}}" style="max-width: 200px; max-height: 200px;" alt="#">
                            </div>
                            <div class="profile-all-dt">
                                <div class="profile-name-dt">
                                    <h1></h1>
                                    <p>
                                        <span><i class="fas fa-map-marker-alt"></i></span>
                                        {{auth()->user()->doctor->address}},
                                        {{isset(auth()->user()->doctor->city->name)?auth()->user()->doctor->city->name:''}}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--my-account end-->
    <!--my-account-tabs start-->
    <section class="all-profile-details">
        <div class="container">
            <div class="row">
                <!----- sidebar section ------>
                <div class="col-lg-3 col-md-4 col-12">
                    <div class="left-tab-links">
                        <div class="nav nav-pills nav-stacked nav-tabs ui vertical menu fluid">
                            <a href="#about" data-toggle="tab" class="item user-tab cursor-pointer">About</a>
                            <a href="#edit-profile" data-toggle="tab" class="item user-tab cursor-pointer">Edit</a>
                            <a href="#edit-chamber" data-toggle="tab" class="item user-tab cursor-pointer">Chamber</a>
                            <a href="#edit-acivement" data-toggle="tab" class="item user-tab cursor-pointer">Achivement</a>
                            <a href="#change-password" data-toggle="tab" class="item user-tab cursor-pointer">Change Password</a>
                        </div>
                    </div>
                </div>
                <!----- sidebar section end------>
                <div class="col-lg-9 col-md-8 col-12">
                    <div class="tab-content">
                        <div class="tab-pane active" id="about">
                            <div class="profile-about">
                                <div class="about-dtp">
                                    <div class="about-bg">
                                        <ul>
                                            <li>
                                                <div class="dp-detail">
                                                    <h6>Ful Name</h6>
                                                    <p>{{auth()->user()->name}}</p>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="dp-detail">
                                                    <h6>Mobile Number</h6>
                                                    <p>{{auth()->user()->phone_number}}</p>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="dp-detail">
                                                    <h6>Email Address</h6>
                                                    <p>{{auth()->user()->email}}</p>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="dp-detail">
                                                    <h6>address</h6>
                                                    <p>
                                                        {{auth()->user()->address}}
                                                    </p>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="edit-profile">
                            <div class="timeline">
                                <div class="edit-profile">

                                    <form id="customer_update_form" enctype="multipart/form-data">
                                        <div class="setting-dt">
                                            <h4>Image</h4>
                                            <div class="avatar">
                                                <img src="#" alt="#">
                                            </div>
                                            <div class="upload-avatar">
                                                <div class="input-heading">Upload a Image</div>
                                                <div class="input-container">
                                                    <input type="file" name="image">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Full Name*</label>
                                            <input type="text" class="video-form" id="name" name="name" value=""  placeholder="Your name">
                                        </div>
                                        <div class="form-group">
                                            <label>Phone Number*</label>
                                            <input type="text" class="video-form" id="phone_number" name="phone_number" value="" placeholder="your phone number">
                                        </div>
                                        <div class="form-group">
                                            <label>Address*</label>
                                            <div class="field-input">
                                                <input type="text" class="video-form" id="address" name="address" value="" placeholder="your address">
                                                <i class="fas fa-search"></i>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>City*</label>
                                            <div class="field-input">
                                                <select name="city" id="city">
                                                    <option value="#">####</option>
                                                </select>
                                            </div>
                                        </div>
                                        <input type="submit" class="btn btn-primary" value="Submit">
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="change-password">
                            <div class="timeline">
                                <div class="edit-profile">
                                    <center><label id="showpassinfo"></label></center>
                                    <form id="customer_password_form">
                                        <div class="form-group">
                                            <label>Old Password*</label>
                                            <input type="password" class="video-form" id="old_password" name="old_password" placeholder="Enter Old Password">
                                        </div>
                                        <div class="form-group">
                                            <label>New Password*</label>
                                            <input type="password" class="video-form" id="password" name="password" placeholder="Enter New Password">
                                        </div>
                                        <div class="form-group">
                                            <label>Confirm Password*</label>
                                            <input type="password" class="video-form" id="password_confirmation" name="password_confirmation" placeholder="Enter Confirm Password">
                                        </div>
                                        <input type="submit" class="btn btn-primary" value="Submit">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--my-account-tabs end-->
@endsection
